import { AuthGuardService } from './services/auth-guard.service';
import { CreateAccountFormComponent } from './components/create-account-form/create-account-form.component';
import { LoginComponent } from './components/login/login.component';
import { EditFormComponent } from './components/edit-form/edit-form.component';
import { CreateFormComponent } from './components/create-form/create-form.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { TableComponent } from './components/table/table.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './components/form/form.component';

const routes: Routes = [
  {
    path: 'login', component: LoginComponent
  }
  ,
  {
    path: 'createAccount', component: CreateAccountFormComponent
  }
  ,
  {
    path: 'home', component: HomeComponent, canActivate: [AuthGuardService]
  }
  ,
  {
    path: 'table', component: TableComponent, canActivate: [AuthGuardService]
  }
  ,
  {
    path: 'create', component: FormComponent, canActivate: [AuthGuardService]
  }
  ,
  {
    path: 'edit/:id', component: FormComponent, canActivate: [AuthGuardService]
  }
  ,
  {
    path: '', redirectTo: '/login', pathMatch: 'full', 
  },
  {
    path: '**', component: NotFoundComponent, 
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
