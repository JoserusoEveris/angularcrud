import { IPeople } from 'src/app/models/people';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})
export class Filter implements PipeTransform {

    transform(people: IPeople[], searchTerm: any): IPeople[] {
        if (!people || !searchTerm) {
            return people
        }

        return people.filter(person =>
            person.id.toString().toLocaleLowerCase().indexOf(searchTerm.toLocaleLowerCase()) !== -1 ||
            person.first_name.toLocaleLowerCase().indexOf(searchTerm.toLocaleLowerCase()) !== -1 ||
            person.last_name.toLocaleLowerCase().indexOf(searchTerm.toLocaleLowerCase()) !== -1 ||
            person.gender.toLocaleLowerCase().indexOf(searchTerm.toLocaleLowerCase()) !== -1 ||
            person.email.toLocaleLowerCase().indexOf(searchTerm.toLocaleLowerCase()) !== -1);
    }
}