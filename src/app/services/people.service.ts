import { IPeople } from './../models/people';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  url: string = '/people/';

  constructor(private http: HttpClient) { }

  getPeople(): Observable<IPeople[]> {
    return this.http.get<IPeople[]>(this.url);
  }

  getPersonByID(id: number): Observable<IPeople> {
    return this.http.get<IPeople>(`${this.url}${id}`);
  }

  createPerson(person): Observable<IPeople> {
    return this.http.post<IPeople>(this.url, person);
  }

  editPerson(data): Observable<IPeople> {
    const endpoint = this.url + data.id;
    return this.http.put<IPeople>(endpoint, data.person);
  }

  deletePerson(id: number): Observable<IPeople> {
    return this.http.delete<IPeople>(`${this.url}${id}`);
  };


}
