import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  setJWT(token: string) {
    localStorage.setItem('jwt', token);
  }

  deleteToken() {
    localStorage.removeItem('jwt');
  }

  getJWT(): string {
    return localStorage.getItem('jwt');
  }

  isLoggedIn():boolean {
    return !!localStorage.getItem('jwt');
  }

}
