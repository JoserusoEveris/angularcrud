import { AuthService } from './auth.service';
import { Observable, throwError } from 'rxjs';
import { map, finalize, catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { apiUrl } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  baseUrl = apiUrl.baseUrl;

  constructor(private authService: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token: string = this.authService.getJWT();
    // Check the token to add it to the headers
    if (token) {
      req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token) })
    }

    // Adds the headers
    if (!req.headers.has('Content-Type')) {
      req = req.clone({ headers: req.headers.set('Content-type', 'application/json; charset=utf-8') })
    }
    req = req.clone({ headers: req.headers.set('Accept', 'application/json') });

    // Creates the new url
    const newUrl = { url: this.baseUrl + req.url };
    req = Object.assign(req, newUrl);
    const newUrlWithParams = { urlWithParams: this.baseUrl + req.urlWithParams };
    req = Object.assign(req, newUrlWithParams);

    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log('event ---> ', event)
        } return event;
      }),
      finalize(() => { })

      // Controls all the errors from the diferents services
    ).pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMsg = '';
        if (error.error instanceof ErrorEvent) {
          console.log('this is client side error');
          errorMsg = `Error: ${error.error.message}`;
        }
        else {
          console.log('this is server side error');
          errorMsg = `Error Code: ${error.status},  Message: ${error.message}`;
        }
        console.log(errorMsg);
        return throwError(errorMsg);
      })
    );

  }

}
