import { IUser } from './../models/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = '/users';

  constructor(private http: HttpClient) { }

  createAccount(user:IUser): Observable<IUser> {
    return this.http.post<IUser>(this.url, user);
  }

  getUsersByID(id: number): Observable<IUser> {
    return this.http.get<IUser>(`${this.url}/${id}`);
  }

}
