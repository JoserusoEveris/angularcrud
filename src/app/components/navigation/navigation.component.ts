import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(private authSerive:AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  logOut(){
    this.authSerive.deleteToken();
    this.router.navigate(['/login'])
  }

}
