import { PeopleService } from '../../services/people.service';
import { IPeople } from '../../models/people';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.scss']
})
export class CreateFormComponent implements OnInit {


  constructor(private fb: FormBuilder, private peopleService: PeopleService, private route: Router) { }

  people: IPeople;
  emailPattern: string = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

  peopleForm: FormGroup = this.fb.group({
    first_name: ['', Validators.required],
    last_name: ['', Validators.required],
    email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
    gender: ['', Validators.required],
    married: [false, Validators.required]
  })

  // GETTERS
  get firstName() { return this.peopleForm.get('first_name'); }
  get lastName() { return this.peopleForm.get('last_name'); }
  get email() { return this.peopleForm.get('email'); }
  get gender() { return this.peopleForm.get('gender'); }

  ngOnInit(): void {
  }

  onFormSubmit(person: IPeople): void {
    this.peopleService.createPerson(person).subscribe(
      newPerson => {
        this.people = newPerson
        Swal.fire(
          'Good job!',
          'You created a register!',
          'success'
        )
        this.route.navigate(['/table'])
        console.log(this.peopleForm.value)
      },
      error => {
        Swal.fire(
          'Error!',
          'We could not create the person!',
          'error'
        )
        console.error(error)
      }
    )
  }

}
