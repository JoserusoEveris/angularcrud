import { AuthService } from './../../services/auth.service';
import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IUser } from 'src/app/models/user';
import { LoginService } from 'src/app/services/login.service';
import Swal from 'sweetalert2';
import { randomHashGenerator } from '../../shared/randomHashGenerator'

@Component({
  selector: 'app-create-account-form',
  templateUrl: './create-account-form.component.html',
  styleUrls: ['./create-account-form.component.scss']
})
export class CreateAccountFormComponent implements AfterViewInit {

  users: IUser;
  user: IUser;

  loginForm: FormGroup = this.fb.group({
    id: ['', Validators.required],
    password: ['', Validators.required]
  });

  @ViewChild('emailRef') emailElementRef: ElementRef;

  constructor(private fb: FormBuilder, private loginService: LoginService,
    private authService: AuthService, private router: Router) { }

  // Getters
  get password() { return this.loginForm.get('password'); }
  get id() { return this.loginForm.get('id'); }


  ngAfterViewInit(): void {
    this.emailElementRef.nativeElement.focus();
  }

  // Creates a new account
  onSubmit() {
    this.users = {
      password: this.password.value,
      id: this.id.value
    }
    console.log(this.users)
    this.loginService.createAccount(this.users).subscribe(
      response => {
        const hash = randomHashGenerator();
        this.authService.setJWT(this.id.value + this.password.value + hash)
        console.log("Success user : ", response);
        this.router.navigate(['/home']);
      },
      error => Swal.fire(
        'Error!',
        'That email has an account already!',
        'error'
      )
    );
  }

}
