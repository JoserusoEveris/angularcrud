import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IPeople } from 'src/app/models/people';
import { PeopleService } from 'src/app/services/people.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit, AfterViewInit {

  constructor(private fb: FormBuilder, private peopleService: PeopleService, private router: Router,
    private route: ActivatedRoute) { }

  // Variables
  people: IPeople;
  isLoading: boolean = true;
  emailPattern: string = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  id: number;
  title: string

  @ViewChild('nameRef') nameElementRef: ElementRef;

  // People form group
  peopleForm: FormGroup = this.fb.group({
    first_name: ['', Validators.required],
    last_name: ['', Validators.required],
    email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
    gender: ['', Validators.required],
    married: [false, Validators.required]
  })

  // Getters
  get firstName() { return this.peopleForm.get('first_name'); }
  get lastName() { return this.peopleForm.get('last_name'); }
  get email() { return this.peopleForm.get('email'); }
  get gender() { return this.peopleForm.get('gender'); }

  ngOnInit(): void {
    // Gets the id from the route
    this.route.paramMap.subscribe(param => {
      const id = +param.get('id');
      this.id = id;
      this.getPerson(id);
    });

    // Checks the id to settle the title
    this.id === 0 ?
      this.title = "Create user" :
      this.title = "Edit user";

  }

  ngAfterViewInit(): void {
    this.nameElementRef.nativeElement.focus();
  }

  // Calls the service passing the given id in the route
  getPerson(id: number) {
    id !== 0 &&
      this.peopleService.getPersonByID(id).subscribe(
        response => {
          this.people = response;
          // Populate the form
          this.peopleForm.patchValue({
            first_name: this.people.first_name,
            last_name: this.people.last_name,
            email: this.people.email,
            gender: this.people.gender,
            married: this.people.married
          });
          this.isLoading = false;
        },
        error => {
          Swal.fire(
            'Error!',
            'We could load the person!',
            'error'
          )
        }
      )
  }

  // Checks the id to create or update
  onSubmit(): void {
    this.id !== 0 ? this.onFormEdit() : this.onFormSubmit();
  }

  // Calls the edit service passing the id and the form value in the object data
  onFormEdit(): void {
    const data = {
      id: this.people.id,
      person: this.peopleForm.value
    }
  
    this.peopleService.editPerson(data).subscribe(
      respose => {
        this.router.navigate(['/table'])
        Swal.fire(
          'Good job!',
          'You edited the person!',
          'success'
        );
      },
      error => {
        Swal.fire(
          'Error!',
          'We could not edit the person!',
          'error'
        );
      }
    )
  }

  // Creates a new person passing the peopleForm to the service
  onFormSubmit(): void {
    this.peopleService.createPerson(this.peopleForm.value).subscribe(
      response => {
        this.people = response
        Swal.fire(
          'Good job!',
          'You created a register!',
          'success'
        );
        this.router.navigate(['/table']);
      },
      error => {
        Swal.fire(
          'Error!',
          'We could not create the person!',
          'error'
        );
      }
    );
  }

}
