import { PeopleService } from './../../services/people.service';
import { IPeople } from './../../models/people';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  // Variables
  public people: IPeople[];
  isLoading: boolean = true;
  searchTerm: any;


  constructor(private peopleService: PeopleService, private router: Router) { }

  ngOnInit(): void {
    // Populate the table calling the service
    this.peopleService.getPeople().subscribe(
      response => {
        this.people = response
        this.isLoading = false;
      },
      error => Swal.fire(
        'Error!',
        'We could not load the data!',
        'warning'
      )
    );
  }

  // Call the delete service passing the row id
  onDelete(id: number) {
    this.peopleService.deletePerson(id).subscribe(
      response => {
        this.people = this.people.filter(x => x.id !== id);
        this.isLoading = false;
        Swal.fire(
          'Good job!',
          'You deleted the person!',
          'success'
        );
      },
      error => Swal.fire(
        'Error!',
        'We could not deleted the person!',
        'error'
      )
    );
  }

  // Navigate to the edit form and pass the id
  editPerson(id: number) {
    this.router.navigate(['/edit', id]);
  }

}
