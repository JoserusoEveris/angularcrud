import { AuthService } from './../../services/auth.service';
import { IUser } from './../../models/user';
import { LoginService } from './../../services/login.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { randomHashGenerator } from 'src/app/shared/randomHashGenerator';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {

  constructor(private fb: FormBuilder, private loginService: LoginService, 
    private authSerive: AuthService, private router: Router) { }

  users: IUser;
  user: IUser;
  fieldTextType: boolean = true;

  @ViewChild('emailRef') emailElementRef: ElementRef;

  loginForm: FormGroup = this.fb.group({
    id: ['', Validators.required],
    password: ['', Validators.required]
  })

  // Getters
  get password() { return this.loginForm.get('password'); }
  get id() { return this.loginForm.get('id'); }


  ngOnInit(): void {
    this.authSerive.isLoggedIn() && this.router.navigate(['home']);
  }
  
  ngAfterViewInit(): void {
    this.emailElementRef.nativeElement.focus();
  }

  // Log in an account checking if exist in the database
  onSubmit() {
    this.loginService.getUsersByID(this.id.value).subscribe(
      response => {
        // Checks if the response from the server and the formValues are equals
        if (JSON.stringify(response.id) == JSON.stringify(this.id.value) &&
        JSON.stringify(response.password) == JSON.stringify(this.password.value)) {
          this.authSerive.setJWT(this.id.value+this.password.value+randomHashGenerator());
          this.router.navigate(['/home']);
        } else {
          Swal.fire(
            'Error!',
            'Incorrect username and/or password!',
            'error'
          )
        }
      },
      error => Swal.fire(
        'Error!',
        'Account not found!',
        'warning'
      )
    )
  }

  // Show or hide the password input
  showHide(){
    this.fieldTextType = !this.fieldTextType;
  }
}
