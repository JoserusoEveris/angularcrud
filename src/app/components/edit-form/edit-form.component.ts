import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IPeople } from 'src/app/models/people';
import { PeopleService } from 'src/app/services/people.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.scss']
})
export class EditFormComponent implements OnInit {

  constructor(private fb: FormBuilder, private peopleService: PeopleService, private router: Router,
    private route: ActivatedRoute) { }

  people: IPeople;
  isLoading: boolean = true;
  emailPattern: string = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

  peopleForm: FormGroup = this.fb.group({
    first_name: ['', Validators.required],
    last_name: ['', Validators.required],
    email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
    gender: ['', Validators.required],
    married: [false, Validators.required]
  })

  // GETTERS
  get firstName() { return this.peopleForm.get('first_name'); }
  get lastName() { return this.peopleForm.get('last_name'); }
  get email() { return this.peopleForm.get('email'); }
  get gender() { return this.peopleForm.get('gender'); }

  ngOnInit(): void {
    this.route.paramMap.subscribe(param => {
      const id = +param.get('id');
      this.getPerson(id);
    });
  }

  getPerson(id: number) {
    this.peopleService.getPersonByID(id).subscribe(
      response => {
        this.people = response;
        this.peopleForm.patchValue({
          first_name: this.people.first_name,
          last_name: this.people.last_name,
          email: this.people.email,
          gender: this.people.gender,
          married: this.people.married
        });
        this.isLoading = false;
        console.log(this.people);
      }
    )
  }

  onFormEdit(): void {
    const data = {
      id: this.people.id,
      person: this.peopleForm.value
    }

    console.log(data)
    this.peopleService.editPerson(data).subscribe(
      newPerson => {
        this.router.navigate(['/table'])
        console.log(this.peopleForm.value);
        Swal.fire(
          'Good job!',
          'You edited the person!',
          'success'
        )
      },
      error => {
        console.error(error)
        Swal.fire(
          'Error!',
          'We could not edit the person!',
          'error'
        )
      }
    )

  }

}
